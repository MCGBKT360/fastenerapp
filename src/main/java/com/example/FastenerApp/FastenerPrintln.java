package com.example.FastenerApp;

import java.io.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import jakarta.servlet.http.HttpServletResponse;

/*
    CIT360-03 Paul McGregor
        References: CIT 360 course material Winter 2020-2021
        BYU-I Tutoring services Cole
        https://ssaurel.medium.com/create-a-simple-http-web-server-in-java-3fc12b29d5fd
        https://mydoto.com/java/how-to-connect-and-retrieve-data-from-a-mysql-database-in-java-a-novice-friendly-guide/
        https://www.javaguides.net/2019/03/jsp-servlet-hibernate-crud-example.html
        https://www.jetbrains.com/help/idea/creating-local-server-configuration.html
        https://youtu.be/QZVosiLT-y8 Example code used from Simple Servlet Example Troy Tuckett
        https://www.tutorialspoint.com/php/mysql_select_php.htm
        https://youtu.be/ToIQFP55s7Q 'How to retrieve data from database and Display in JSP Page?' roseindiatutorials
 */

@WebServlet(name = "fastenerServlet", value = "/fastener-servlet")
public class FastenerPrintln extends HttpServlet {
    private String message;

    public void init() {
        message = "Congratulations Your Item Code Has Been Verified!";
    }

    public void doGet (HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        request.getParameter("itemCode");
        // specifiy it needs to get the item code


        // Item code verified!
        PrintWriter out = response.getWriter();

        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}