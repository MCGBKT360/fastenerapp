<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>

<!-- CIT360-03 Paul McGregor
References: CIT 360 course material Winter 2020-2021
BYU-I Tutoring services Cole
https://ssaurel.medium.com/create-a-simple-http-web-server-in-java-3fc12b29d5fd
https://mydoto.com/java/how-to-connect-and-retrieve-data-from-a-mysql-database-in-java-a-novice-friendly-guide/
https://www.javaguides.net/2019/03/jsp-servlet-hibernate-crud-example.html
https://www.jetbrains.com/help/idea/creating-local-server-configuration.html
https://youtu.be/QZVosiLT-y8 Example code used from Simple Servlet Example Troy Tuckett
https://www.tutorialspoint.com/php/mysql_select_php.htm
https://youtu.be/ToIQFP55s7Q 'How to retrieve data from database and Display in JSP Page?' roseindiatutorials -->


<html>
<head>
    <title>Fastener Item Code Lookup</title>
    <script>
        function validateForm() {
            const itemCode = document.forms["itemCodeEntry"]["itemCode"].value;
            if (itemCode.length !== 4) {
                alert("Entry must be exactly 4 digits long!");
                return false;
            }
        }
    </script>
</head>
<body>
<header>
    <h1><%= "Fastener Product lookup" %></h1>
</header>
<br/>
<a href="fastener-servlet">Fastener App Servlet</a>
<fieldset>
    <legend>Lookup any 4 digit Fastener item code here!<br/>
            Below is a list of available items for you to choose from...<br/>
            1001, 1002, 1003, 1004<br/>
            Please make your selection...
    </legend>
    <form name="itemCodeEntry" onsubmit="return validateForm()" action="fastener-servlet" method="get">
        Item code:<label>
        <input type="number" name="itemCode" placeholder="0000">
        <href></href>
    </label>
        <br/>
        <input type="submit" value="Submit">
    </form>
</fieldset>
<p align="center"><button type="submit" class="btn btn-primary btn-default" id="fasterers">Submit</button></p>


</body>
</html>